#include <esp_now.h>
#include <ArduinoJson.h>

#ifndef Arduino_h
#include <Arduino.h>
#endif

#ifndef GrocyFunctions_h
#include "GrocyFunctions.h"
#endif

int GRAM_UNITS[] = {17, 16}; // g & ml
int KILOGRAM_UNITS[] = {10};

grocyProduct waiting_product;
String waiting_product_barcode;
int waiting_product_timeout;
float waiting_product_consume_amount;
bool product_waiting = false;

typedef struct struct_message {
  int weight;
} struct_message;

struct_message myData;

esp_now_peer_info_t peerInfo;

class WeightModuleClient {
    public:
        void weightModuleExecuteLateConsume();
        void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status);
        void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len);
        bool weightModuleShouldWait(grocyProduct product, String barcode, long fallback_consume_amount);
        void weightModuleCheckTimeout();

        WeightModuleClient(Grocy& grocyClient, CheckoutPartnerDisplay& deviceScreen)
        :_grocyServer(grocyClient), _deviceScreen(deviceScreen)
        {
        };

    private:
        Grocy& _grocyServer;
        CheckoutPartnerDisplay& _deviceScreen;
};

void WeightModuleClient::weightModuleExecuteLateConsume() {
    Serial.println("Going to consume: "+String(waiting_product_consume_amount)+" of "+waiting_product_barcode);

    JsonDocument postParams;
    postParams["amount"] = waiting_product_consume_amount;
    postParams["transaction_type"] = "consume";
    postParams["spoiled"] = false;

    String serializedPostParams;
    serializeJson(postParams, serializedPostParams);

    webResponse response = _grocyServer.makeWebRequest("/api/stock/products/by-barcode/"+waiting_product_barcode+"/consume", serializedPostParams);

    if (response.statusCode == HTTP_CODE_OK) {
        // Screen stock update can deal with negative values for amount open
        _deviceScreen.screenStockUpdate(String(waiting_product_consume_amount) + "x " + waiting_product.name,
                                    waiting_product.stock_amount - waiting_product_consume_amount,
                                    waiting_product.stock_amount_opened - waiting_product_consume_amount);
        
        digitalWrite(BUZZER_PIN, HIGH);
        delay(25);
        digitalWrite(BUZZER_PIN, LOW);
    } else {
        _deviceScreen.screenUpdate("Failed with status: "+response.statusCode);
    }

    product_waiting = false;
}

void WeightModuleClient::OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
    memcpy(&myData, incomingData, sizeof(myData));
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("Weight: ");
    Serial.println(myData.weight);
  
    bool isKG = false;
    float weight = myData.weight;
    for (int i = 0; i < sizeof(KILOGRAM_UNITS)/sizeof(int); i++) {
        if (waiting_product.qu_id_stock == KILOGRAM_UNITS[i]) {
            isKG = true;
        }
    }

    if (isKG) {
        weight = weight / 1000.0;
        Serial.println("Product is measured in kg. Mass now: "+String(weight));
    }
    waiting_product_consume_amount = waiting_product.stock_amount_opened - weight;
    waiting_product_timeout = millis() + 100;  // Reduce time until action is executed.
    // Can't execute action immediately for some reason (maybe due to ESPNow interfering with WiFi).
}

bool WeightModuleClient::weightModuleShouldWait(grocyProduct product, String barcode, long fallback_consume_amount) {
    bool isRelevantUnit = false;
    for (int i = 0; i < sizeof(GRAM_UNITS)/sizeof(int); i++) {
        if (product.qu_id_stock == GRAM_UNITS[i]) {
            isRelevantUnit = true;
        }
    }

    for (int i = 0; i < sizeof(KILOGRAM_UNITS)/sizeof(int); i++) {
        if (product.qu_id_stock == KILOGRAM_UNITS[i]) {
            isRelevantUnit = true;
        }
    }

    if (isRelevantUnit) {
        // In case the user scans a second item before the first has been checked out
        if (product_waiting) {
            weightModuleExecuteLateConsume();
        }

        waiting_product = product;
        waiting_product_barcode = barcode;
        waiting_product_consume_amount = fallback_consume_amount;
        product_waiting = true;
        waiting_product_timeout = millis() + 120000;
        return true;
    } else {
        Serial.println("Not relevant qu: "+String(product.qu_id_stock));
        return false;
    }
}

void WeightModuleClient::weightModuleCheckTimeout() {
    if (product_waiting && millis() > waiting_product_timeout) {
        Serial.println("Checking in product now");
        weightModuleExecuteLateConsume();
        product_waiting = false;
    }
}