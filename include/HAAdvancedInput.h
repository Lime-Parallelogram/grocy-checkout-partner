#include <Arduino.h>

#include <ArduinoJson.h>

#include "GrocyFunctions.h"

String last_changed;

float haInputAmount(Grocy& grocyServer) {
    JsonDocument filter;
    filter["state"] = true;
    filter["last_changed"] = true;

    webResponse response = grocyServer.makeFilteredWebRequest("/hainput", "", filter);
    float consume_amount = response.resultBody["state"];

    if (response.resultBody["last_changed"] != last_changed) {
        last_changed = response.resultBody["last_changed"].as<String>();
        return consume_amount;
    } else {
        return 0;
    }
}