#include <Arduino.h>

#include <HTTPClient.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>

#ifndef GrocyFunctions_h
#define GrocyFunctions_h

typedef struct {
  int statusCode;
  JsonDocument resultBody;
} webResponse;

typedef struct {
    int id;
    int product_id;
    String barcode;
    float amount;
} product_barcode;

typedef struct {
    int product_id;
    String name;
    float quick_consume_amount;
    float quick_open_amount;
    float stock_amount;
    float stock_amount_opened;
    int qu_id_stock;
    bool enable_tare_weight_handling;
    float tare_weight;
} grocyProduct;

class Grocy {
    public:
    webResponse makeFilteredWebRequest(String path, String postData, JsonDocument resultFilter);
    webResponse makeWebRequest(String path, String postData);
    grocyProduct httpGetProductInfo(String barcode);

    Grocy(WiFiClient client, String httpBasePath, String grocyAPIKey)
    : _httpClient() // Initialise HTTP Client
    {
        _client = client;
        _httpBasePath = httpBasePath;
        _grocyAPIKey = grocyAPIKey;

    }
    
    private:
        WiFiClient _client;
        HTTPClient _httpClient;
        String _grocyAPIKey;
        String _httpBasePath;
};

webResponse Grocy::makeWebRequest(String path, String postData) {
    JsonDocument emptyFilter;
    return makeFilteredWebRequest(path, postData, emptyFilter);
}

webResponse Grocy::makeFilteredWebRequest(String path, String postData, JsonDocument resultFilter) {
    JsonDocument result;

    _httpClient.useHTTP10(true); // Allows for direct decoding of string from response
    if (_httpClient.begin(_client, _httpBasePath+path)) {
        _httpClient.addHeader("GROCY-API-KEY", _grocyAPIKey);
        _httpClient.addHeader("Content-Type", "application/json");
        
        int httpCode;
        if (postData == "") {
        httpCode = _httpClient.GET(); // Error here not of major concern: https://arduino.stackexchange.com/questions/92477/wificlient-cpp517-flush-fail-on-fd-48-errno-11-no-more-processes-what-i
        } else {
        httpCode = _httpClient.POST(postData);
        }
        
        if (resultFilter.isNull()) {
        deserializeJson(result, _httpClient.getStream());
        } else {
        deserializeJson(result, _httpClient.getStream(), DeserializationOption::Filter(resultFilter));
        }
        

        _httpClient.end();

        //// serializeJsonPretty(result, Serial); // Debug only

        return webResponse {
        statusCode: httpCode,
        resultBody: result,
        };
    }

    return webResponse {
        statusCode: -1,
        resultBody: result,
    };
}

grocyProduct Grocy::httpGetProductInfo(String barcode) {
    JsonDocument filter;
    filter["product"]["id"] = true;
    filter["product"]["name"] = true;
    filter["product"]["quick_consume_amount"] = true;
    filter["product"]["qu_id_stock"] = true;
    filter["product"]["quick_open_amount"] = true;
    filter["product"]["enable_tare_weight_handling"] = true;
    filter["product"]["tare_weight"] = true;
    filter["stock_amount"] = true;
    filter["stock_amount_opened"] = true;
    filter["product_barcodes"] = true;

    webResponse response = makeFilteredWebRequest("/api/stock/products/by-barcode/"+barcode, "", filter);

    if (response.statusCode != HTTP_CODE_OK) {
        throw std::invalid_argument("Grocy Server Error");
    }
    
    return grocyProduct {
        product_id: response.resultBody["product"]["id"],
        name: response.resultBody["product"]["name"],
        quick_consume_amount: response.resultBody["product"]["quick_consume_amount"],
        quick_open_amount: response.resultBody["product"]["quick_open_amount"],
        stock_amount: response.resultBody["stock_amount"],
        stock_amount_opened: response.resultBody["stock_amount_opened"],
        qu_id_stock: response.resultBody["product"]["qu_id_stock"],
        enable_tare_weight_handling: response.resultBody["product"]["enable_tare_weight_handling"],
        tare_weight: response.resultBody["product"]["tare_weight"],
    };
}

#endif