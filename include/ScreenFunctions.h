#include <Arduino.h>
#include <SSD1306Wire.h>

#ifndef ScreenFunctions_h
#define ScreenFunctions_h // Protection against being defined twice

class CheckoutPartnerDisplay {
    public:
    CheckoutPartnerDisplay();
    void modeChange(String newMode);
    void begin();
    void screenUpdate(String content);
    void screenNotify(String content);
    void screenStockUpdate(String product, float stock, float open);

    private:
    SSD1306Wire _display;
    String _displayedMode;
};

CheckoutPartnerDisplay::CheckoutPartnerDisplay()
:_display(0x3c, 23, 19) // Initialisation List for display class
{}

// Screen initialisation process cannot be conducted on instantiation because hardware not ready
void CheckoutPartnerDisplay::begin()
{
  _display.init();
  _display.clear();
}

void CheckoutPartnerDisplay::modeChange(String newMode) {
    _displayedMode = newMode;
    screenUpdate("");
}

void CheckoutPartnerDisplay::screenStockUpdate(String product, float stock, float open) {
  // Logic to show correct open amount on screen
  String remainingMessage = "No Remaining Stock";
  if (open > 0) {
    remainingMessage = "Remaining: " + String(stock).substring(0,3) + " (" + String(open).substring(0,3) + " open)";
  } else if (stock > 0) {
    remainingMessage = "Remaining: " + String(stock).substring(0,3);
  }
  screenUpdate(product+"\n" + remainingMessage);
}

void CheckoutPartnerDisplay::screenUpdate(String extra="") {
  _display.clear();
  _display.setFont(ArialMT_Plain_16);
  _display.drawString(0,0, "Mode: "+_displayedMode);
  _display.setFont(ArialMT_Plain_10);
  _display.drawString(0,25, extra);                                                                                                                                                                                                                                                                                          
  _display.display();
}

void CheckoutPartnerDisplay::screenNotify(String content) {
  _display.clear();
  _display.setFont(ArialMT_Plain_16);
  _display.drawString(0,0, "-- System --");
  _display.setFont(ArialMT_Plain_10);
  _display.drawString(0,25, content);
  _display.display();
}
#endif