#include <Arduino.h>

typedef void (*procedureCall)(void);
typedef void (*barcodeActionCall)(String);

typedef struct {
  String name;
  int button_pin;
  int led_pin;
  procedureCall button_action;
  barcodeActionCall scan_action;
} Mode;