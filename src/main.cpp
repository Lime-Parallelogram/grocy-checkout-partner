/*
    Grocy Checkout

    Describe what it does in layman's terms.  Refer to the components
    attached to the various pins.

    Created: 20th Jan 2024
    By: Will Hall

    Modified day month year
    By author's name

*/
#include <Arduino.h>

#include <BluetoothSerial.h>
#include <WiFiClientSecure.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#include "ModeSelect.h" // Holds type definitions for the mode selector
#include "Secrets.h"
#include "GrocyFunctions.h"
#include "ScreenFunctions.h"

#define BUZZER_PIN 18

//// #define USE_HTTPS // Uncomment to use HTTPS Requests
#ifdef USE_HTTPS
  #include "MyCert.h"
  WiFiClientSecure client;
  String HTTP_REQUEST_PREFIX = "https://";
  client.setCACert(GROCY_HTTPS_CERT);
#else
  WiFiClient client;
  String HTTP_REQUEST_PREFIX = "http://";
#endif

// #define USE_HA_ADVANCED_INPUT
#ifdef USE_HA_ADVANCED_INPUT
  #include "HAAdvancedInput.h"
#endif

#define USE_WIGHT_MODULE
#ifdef USE_WIGHT_MODULE
  #include "WeightModuleClient.h"
#endif

// Bluetooth Configuration Options
const bool BT_USE_MAC = true; // Connect using device MAC Address not name
const String BT_DEVICE_NAME = "BARCODE SCANNER";
const String BT_MY_NAME = "Grocy Checkout Partner"; // How other devices will see your ESP32
const BTAddress BT_DEVICE_MAC = BTAddress("03:79:6C:6E:84:B4"); // Ignore if not using MAC Mode
const int BT_CONNECT_CYCLE = 10000; // How often to attempt to reconnect
bool BTConnected = false;

BluetoothSerial SerialBT;

const int WIFI_RECONNECT_TIME = 10000; // How often to check if WiFi is still connected

// WIFI_SSID & WIFI_PASSWORD Provided in Secrets.h

// GROCY_API_KEY & GROCY_HOST Provided in Secrets.h
Grocy grocyServer(client, HTTP_REQUEST_PREFIX+GROCY_SERVICE_HOST, GROCY_API_KEY);

// Setup OLED Screen
CheckoutPartnerDisplay deviceScreen;

#ifdef USE_WIGHT_MODULE
WeightModuleClient weightModuleClient(grocyServer, deviceScreen);
#endif

// Setup device functionality modes
const int AVAILABLE_MODES = 3; // Change this to reflect the number of modes your device supports
const int STARTING_MODE = 0;

Mode modes[AVAILABLE_MODES];
int activeMode = -1; // Matches selectedMode after onChange actions performed
int selectedMode = STARTING_MODE; // Changes when button pressed

// Define prototype functions for your BarcodeScannedFunctions
void barcodeScanConsume(String barcode);
void barcodeScanOpen(String barcode);
void barcodeScanShoppingList(String barcode);

// Define all available modes here
void modeDefinitions(Mode modes[]) {
  // Format: { "Name", ButtonPin, LEDPin, ModeChangeHandler, BarcodeScannedFunction }
  modes[0] = {"Consume", 27, 12, [](){ selectedMode=0; }, barcodeScanConsume}; // Uses lambda function to change value inline
  modes[1] = {"Open", 33, 14, [](){ selectedMode=1; }, barcodeScanOpen};
  modes[2] = {"Shopping", 32, 26, [](){ selectedMode=2; }, barcodeScanShoppingList};
}

//
// ─── BARCODE ON-SCAN ACTION DEFINTIONS ──────────────────────────────────────────
//
void executeConsume(grocyProduct product, long consumeAmount, String barcode) {
  JsonDocument postParams;
  postParams["amount"] = consumeAmount;
  postParams["transaction_type"] = "consume";
  postParams["spoiled"] = false;

  String serializedPostParams;
  serializeJson(postParams, serializedPostParams);

  webResponse response = grocyServer.makeWebRequest("/api/stock/products/by-barcode/"+barcode+"/consume", serializedPostParams);

  if (response.statusCode == HTTP_CODE_OK) {
    // Screen stock update can deal with negative values for amount open
    deviceScreen.screenStockUpdate(String(consumeAmount) + "x " + product.name,
                                  product.stock_amount - consumeAmount,
                                  product.stock_amount_opened - consumeAmount);
    
    digitalWrite(BUZZER_PIN, HIGH);
    delay(25);
    digitalWrite(BUZZER_PIN, LOW);
  } else {
    deviceScreen.screenUpdate("Unknown Server Error");
  }
}

void barcodeScanConsume(String barcode) {
  Serial.println("Consume "+barcode);
  try {
    grocyProduct product = grocyServer.httpGetProductInfo(barcode);

    if (product.stock_amount != 0) {
      float consumeAmount = product.quick_consume_amount;

      // Consume all remaining product if amount remaining is less than quick consume
      if (consumeAmount > product.stock_amount) {
        consumeAmount = product.stock_amount;
      }

      #ifdef USE_HA_ADVANCED_INPUT
      float haConsumeAmount = haInputAmount(grocyServer);
      if (haConsumeAmount != 0) {
        consumeAmount = haConsumeAmount;
      }
      #endif

      bool send_now = true;
      #ifdef USE_WIGHT_MODULE
      send_now = !weightModuleClient.weightModuleShouldWait(product, barcode, consumeAmount);
      #endif

      if (send_now) {
        executeConsume(product, consumeAmount, barcode);
      } else {
        deviceScreen.screenNotify("Weigh Product Now");
      }

      
    } else {
      deviceScreen.screenStockUpdate(product.name, 0, 0);
    } 
  } catch (std::invalid_argument) {
    deviceScreen.screenNotify("Product not known");
  }
  

}

void barcodeScanOpen(String barcode) {
  Serial.println("Open "+barcode);
  try {
    grocyProduct product = grocyServer.httpGetProductInfo(barcode);

    if (product.stock_amount != 0) {
      float openAmount = product.quick_open_amount;

      // Open remaining product if remaining amount less than quick open
      if (openAmount > (product.stock_amount-product.stock_amount_opened)) {
        openAmount = (product.stock_amount-product.stock_amount_opened);
      }

      if (openAmount == 0) {  // If everything already open
        deviceScreen.screenStockUpdate(product.name,
                                      product.stock_amount,
                                      product.stock_amount_opened + openAmount);
      } else {
        #ifdef USE_HA_ADVANCED_INPUT
        float haOpenAmount = haInputAmount(grocyServer);
        if (haOpenAmount != 0) {
          openAmount = haOpenAmount;
        }
        #endif

        JsonDocument postParams;
        postParams["amount"] = openAmount;

        String serializedPostParams;
        serializeJson(postParams, serializedPostParams);

        webResponse response = grocyServer.makeWebRequest("/api/stock/products/by-barcode/"+barcode+"/open", serializedPostParams);

        if (response.statusCode == HTTP_CODE_OK) {
          // Screen stock update can deal with negative values for amount open
          deviceScreen.screenStockUpdate(String(openAmount) + "x " + product.name,
                                        product.stock_amount,
                                        product.stock_amount_opened + openAmount);
          
          digitalWrite(BUZZER_PIN, HIGH);
          delay(25);
          digitalWrite(BUZZER_PIN, LOW);
        } else {
          deviceScreen.screenUpdate("Unknown Server Error");
        }
      }
    } else {
      deviceScreen.screenStockUpdate(product.name, 0, 0);
    }
  } catch (std::invalid_argument) {
    deviceScreen.screenNotify("Product not known");
  }
}

void barcodeScanShoppingList(String barcode) {
  Serial.println("Add to shopping list: "+barcode);
  try {
    grocyProduct product = grocyServer.httpGetProductInfo(barcode);

    JsonDocument postParams;
    postParams["product_id"] = product.product_id;
    postParams["list_id"] = 1;
    postParams["product_amount"] = 1;
    postParams["note"] = "Added via Grocy Checkout Partner";

    String serializedPostParams;
    serializeJson(postParams, serializedPostParams);

    webResponse response = grocyServer.makeWebRequest("/api/stock/shoppinglist/add-product", serializedPostParams);

    if (response.statusCode == HTTP_CODE_NO_CONTENT) {
      // Screen stock update can deal with negative values for amount open
      deviceScreen.screenUpdate(product.name + "\n Added to shopping list");
      
      digitalWrite(BUZZER_PIN, HIGH);
      delay(25);
      digitalWrite(BUZZER_PIN, LOW);
    }

  } catch (std::invalid_argument) {
    deviceScreen.screenNotify("Product not known");
  }
}

//
// ─── BLUETOOTH DEVICE HANDLING ──────────────────────────────────────────────────
//
// Callback when new bluetooth device is discovered
void BTDeviceDiscovered(BTAdvertisedDevice *device) {
  Serial.println("Found Device");
  if (BT_USE_MAC) {
    if ((*device).getAddress().equals(BT_DEVICE_MAC)) {
      SerialBT.discoverAsyncStop();
      Serial.println((*device).getAddress());
      BTConnected = SerialBT.connect(BT_DEVICE_MAC);

      digitalWrite(BUZZER_PIN, HIGH);
      delay(50);
      digitalWrite(BUZZER_PIN, LOW);
    }
  } else if (((*device).getName() == BT_DEVICE_NAME.c_str()) ) {
    BTConnected = SerialBT.connect(BT_DEVICE_NAME);
  }
}

//
// ─── MAIN SETUP PROCEDURE ───────────────────────────────────────────────────────
//
void setup() {
  deviceScreen.begin();
  deviceScreen.screenNotify("Starting.....");

  Serial.begin(9600);
  delay(2500);

  Serial.print("MAC Address is : ");  Serial.println(WiFi.macAddress());
  
  pinMode(BUZZER_PIN, OUTPUT);

  modeDefinitions(modes); // Add all function definitions to modes Array

  // Configure buttons and LED for all defined modes
  for (int i = 0; i < AVAILABLE_MODES; i++) {
    pinMode(modes[i].led_pin, OUTPUT);
    digitalWrite(modes[i].led_pin, LOW);
    pinMode(modes[i].button_pin, INPUT);
    touchAttachInterrupt(modes[i].button_pin, modes[i].button_action, 10);
  }

  // Setup WiFi connection
  WiFi.mode(WIFI_AP_STA);
  deviceScreen.screenNotify("Connecting to WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (!WiFi.isConnected()) {
    delay(2000);
  }

  deviceScreen.screenNotify("WiFi Connected");

  // Make initial request to HA to store current value of last_changed
  #ifdef USE_HA_ADVANCED_INPUT
  haInputAmount(grocyServer);
  #endif

  #ifdef USE_WIGHT_MODULE
  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  // Have to use this lambda function to allow me to use the object.
  esp_now_register_recv_cb(esp_now_recv_cb_t([](const uint8_t *mac, const uint8_t *incomingData, int len) {
    weightModuleClient.OnDataRecv(mac, incomingData, len);
  }));
  #endif

  //Setup bluetooth connection
  Serial.println("Starting Connection Process");
  SerialBT.begin(BT_MY_NAME, true);
}

//
// ─── MAIN EXECUTION LOOP ────────────────────────────────────────────────────────
//
int last_connection_attempt = 0; // Needed to only attempt BT connection periodically
int last_wifi_check = 0;
String bt_serial_content = ""; // Store up serial content until terminator character seen

void loop() {
  // Handle changing selected mode
  if (selectedMode != activeMode) {
    digitalWrite(modes[activeMode].led_pin, LOW);
    activeMode = selectedMode;
    digitalWrite(modes[activeMode].led_pin, HIGH);
    deviceScreen.modeChange(modes[activeMode].name);
    
    digitalWrite(BUZZER_PIN, HIGH);
    delay(50);
    digitalWrite(BUZZER_PIN, LOW);
  }

  // Handle incoming bluetooth serial data from device
  if (SerialBT.available()) {
    char character = SerialBT.read();
    if (character == '\t') { // Set device to TAB terminator
      modes[selectedMode].scan_action(bt_serial_content);
      bt_serial_content = "";
    } else {
      bt_serial_content.concat(character);
    }
  }

  // Attempt to connect to scanner every 2.5s
  if ((millis() - last_connection_attempt) > BT_CONNECT_CYCLE) {
    if (!SerialBT.connected()) {
      SerialBT.discoverAsync(BTDeviceDiscovered, BT_CONNECT_CYCLE);
    }
    last_connection_attempt = millis();
  }

  // Check WiFi
  if ((millis() - last_wifi_check) > WIFI_RECONNECT_TIME) {
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("Reconnecting WiFi");
      WiFi.disconnect();
      WiFi.reconnect();
    }
    last_wifi_check = millis();
  }

  #ifdef USE_WIGHT_MODULE
  weightModuleClient.weightModuleCheckTimeout();
  #endif

  delay(30);
}